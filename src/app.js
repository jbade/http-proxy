const fs = require('fs');
const ProxyServer = require('./server/ProxyServer.js');
const path = require('path');
const Submitter = require('./util/Submitter');
const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');

var logDirectory = path.join(__dirname, '../log')
if (!fs.existsSync(logDirectory))
    fs.mkdirSync(logDirectory);

var logger = new winston.Logger({
    exitOnError: false,
    transports: [
        new DailyRotateFile({
            name: 'proxy',
            datePattern: '.yyyy-MM-dd-HH',
            filename: path.join(logDirectory, 'proxy.log')
        }),
        new winston.transports.Console({
            colorize: true
        })
    ]
});

var proxy = new ProxyServer({
    logger: logger,
    servers: [{
        port: 8080,
        https: true
    }],
    throttle: {
        // bandwidth: {
            server: { // TODO: have option to pass ThrottleGroup object
                // 100 Mbps
                burstRate: 125000, // bytes/s
                fillRate: 125000, // bytes/s
                chunksize: 4096 // bytes
            },
            user: {
                // 1 Mb burst, 0.1 Mb fill
                burstRate: 125000, //125000, // bytes/s
                fillRate: 125000, //12500, // bytes/s
                chunksize: 4096 // bytes
            }
        },
        requests: {
            user: {
                burstRate: 200, // requests
                fillRate: 30, // requests
                interval: 1000 // ms
            }
        },
        filesize: {
            // 4 MB
            request: 40000000, // bytes
            response: 40000000 // bytes
        },
        transfer: {
            user: {
                // 64 MB per 10 minutes
                size: 64000000, // bytes
                interval: 10 * 60 * 1000 // ms
            }
        },
        // applies to bandwith, requests. does not apply to filesize, transfer
        exception: {
            contentTypes: ['javascript'],
            fileTypes: ['js'],
        }
    },
    splash: {
        // be sure to whitelist all on splashpage
        url: 'http://example.com/splash',
        key: '',
        interval: 10 * 60 * 1000
    },
    redirects: {
        blocked: 'http://example.com/blocked',
        transferlimit: 'http://example.com/transferlimit',
        requestlimit: 'http://example.com/requestlimit',
        filesizelimit: 'http://example.com/filesizelimit'
    },
    // regex
    whitelist: {
        urls: [
            ['^http:\/\/example\.com\/splash$', 'i']
        ],
        hosts: [
            ['^(?:ipv4|www|safebrowsing|safebrowsing-cache)\.google\.com$', 'i']
        ]
    },
    blocked: {
        hosts: [
            ['.gov(?:\.[a-zA-Z]{1,3}){0,2}$', ''] // government domains
        ],
        urls: []
    }
});

 // modify js
proxy.modify('prepend-response-body', {
    contentTypes: {
        request: ['javascript'],
        response: ['javascript']
    },
    fileTypes: ['js']
}, (req, proxyRes) => {
    return 'console.log("prepend-response-body");';
});

// modify html
proxy.modify('response-body', {
    contentTypes: {
        request: ['html', 'text'],
        response: ['html', 'text']
    }
}, (req, proxyRes, chunk, encoding) => {
    return chunk.toString().replace(/<head>/, '<head>' + '<p>head</p>')
        .replace(/<\/body>/, '<p>body</p>' + '</body>');
});

proxy.modify('request-headers', {}, (req) => {
    req.headers['x-forwarded-for'] = req.connection.remoteAddress;
});

proxy.modify('request-headers', {
    contentTypes: ['javascript'],
    fileTypes: ['js']
}, (req) => {
    // we want new content
    delete req.headers['if-modified-since'];
    delete req.headers['if-none-match'];
    delete req.headers['etag'];

    // this helps
    if ((req.headers['cache-control'] || '').indexOf('no-cache') !== -1) {
        req.headers['cache-control'] = 'no-cache';
    } else {
        delete req.headers['cache-control'];
    }
});

proxy.modify('response-headers', {
    contentTypes: ['javascript'],
    fileTypes: ['js']
}, (req, res) => {
    // attempt to cache file forever
    res.headers['expires'] = new Date(Date.now() + 31556926).toUTCString(); // max
    //res.headers['last-modified'] = new Date().toUTCString();
    //res.headers['date'] = new Date().toUTCString();

    // I don't know if this helps
    if ((res.headers['cache-control'] || '').indexOf('no-cache') !== -1) {
        res.headers['cache-control'] = 'public, max-age=31556926, no-cache';
    } else {
        res.headers['cache-control'] = 'public, max-age=31556926';
    }
});
