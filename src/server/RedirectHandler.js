/**
 * The redirect handler is responsible for sending responses to the user that
 * tell it to redirect to a different url than what was requested. These redirections
 * are used to infrom the user on why their request has been rejected. The redirection
 * types are redirection to a splash page to provide information about the proxy,
 * redirection of blocked requests, and redirection to inform the user that they hit
 * various limits in terms of requests/total transfer/file size.
 * @param {Object} settings
 */
function RedirectHandler(settings) {

    /**
     * Redirects the request to the provided url if valid
     * @param  {http.Response} res
     * @param  {String} url
     */
    this.redirectHttp = (res, url) => {
        if (typeof url === 'string') {
            res.writeHead(302, {
                'Location': url
            });
        } else {
            res.writeHead(403);
        }
        res.end();
    }

    /**
     * Redirects the request to the provided url if valid
     * @param  {http.Response} res
     * @param  {String} url
     */
    this.redirectHttps = (req, clientSocket, url) => {
        if (typeof url === 'string') {
            clientSocket.write('HTTP/' + req.httpVersion + ' 302 Found\r\nLocation: ' + url + '\r\n');
        } else {
            clientSocket.write('HTTP/' + req.httpVersion + ' 403 Forbidden\r\n\r\n');
        }
        clientSocket.end();
    }

    this.splash = (reqRes, clientSocket) => {
        if (typeof clientSocket === 'undefined')
            this.redirectHttp(reqRes, (settings.splash || {}).url);
        else
            this.redirectHttps(reqRes, clientSocket, (settings.splash || {}).url);
    }

    this.blocked = (reqRes, clientSocket) => {
        if (typeof clientSocket === 'undefined')
            this.redirectHttp(reqRes, (settings.redirects || {}).blocked);
        else
            this.redirectHttps(reqRes, clientSocket, (settings.redirects || {}).blocked);
    }

    this.transferLimit = (reqRes, clientSocket) => {
        if (typeof clientSocket === 'undefined')
            this.redirectHttp(reqRes, (settings.redirects || {}).transferlimit);
        else
            this.redirectHttps(reqRes, clientSocket, (settings.redirects || {}).transferlimit);
    }

    this.requestLimit = (reqRes, clientSocket) => {
        if (typeof clientSocket === 'undefined')
            this.redirectHttp(reqRes, (settings.redirects || {}).requestlimit);
        else
            this.redirectHttps(reqRes, clientSocket, (settings.redirects || {}).requestlimit);
    }

    this.fileSizeLimit = (reqRes, clientSocket) => {
        if (typeof clientSocket === 'undefined')
            this.redirectHttp(reqRes, (settings.redirects || {}).filesizelimit);
        else
            this.redirectHttps(reqRes, clientSocket, (settings.redirects || {}).filesizelimit);
    }

}
module.exports = RedirectHandler;
