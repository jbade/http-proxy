const net = require('net');
const ip = require('ip');

const getHostPortFromString = require('./util/HostPort.js');

/**
 * This module is used to add https proxy support to the http proxy server. It is
 * done so by listening for socket connects on the same httpserver as the http proxy
 * is listening on. Requests made through https are subjected to a majority of the
 * constaints put on requests made through http.
 * @param {Object} settings
 * @param {httpServer} server
 * @param {ProxyServer} proxyServer
 */
function addHttpsProxy(settings, server, proxyServer) {
    server.addListener('connect', (req, clientSocket, bodyhead) => {
        // don't accept nonhttps proxy requests
        if (typeof req.headers['host'] === 'undefined') {
            clientSocket.end();
            return;
        }

        var requestIp = req.connection.remoteAddress || req.socket.remoteAddress ||
            req.connection.socket.remoteAddress || clientSocket.remoteAddress;

        if (typeof settings.logger !== 'undefined')
            settings.logger.log('info', {
                type: 'request',
                ip: requestIp,
                host: req.headers['host']
            });

        var user = proxyServer.userTable.get(requestIp);
        var hostPort = getHostPortFromString(req.headers['host'], 443);
        var whitelisted = proxyServer.accessHandler.isWhitelisted(req, hostPort);


        //console.log(whitelisted);
        // if (!whitelisted) {
        //     clientSocket.end();
        //     return;
        // }





        // handle blocking
        if (proxyServer.accessHandler.isBlocked(req, hostPort, server.address().address, whitelisted)) {
            proxyServer.redirectHandler.blocked(req, clientSocket);
            return;
        }

        // handle splash
        if (!whitelisted && proxyServer.splashHandler.shouldSplash(user)) {
            proxyServer.redirectHandler.splash(req, clientSocket);
            return;
        }

        // handle transfer limits
        if (!whitelisted && user.transferTracker.isLimited()) {
            // TODO: redirect to limited page
            //console.log('TransferTracker limit hit https');
            proxyServer.redirectHandler.transferLimit(req, clientSocket);
            return;
        }

        user.requestLimiter.removeTokens(1, (err, remainingRequests) => {
            if (!whitelisted && (err || remainingRequests < 0)) { // cancel request if hit rate limit
                //console.log('requestLimiter user ', err, remainingRequests);
                proxyServer.redirectHandler.requestLimit(req, clientSocket);
                return;
            }

            var destinationSocket = new net.Socket();
            var cancelRequest = () => {
                clientSocket.end();
                destinationSocket.end();
            };
            var pipesettings = {
                end: true
            };

            var pipe = destinationSocket.pipe(proxyServer.fileSizeHandler.createResponseStream(cancelRequest), pipesettings); // maxBodySize checker

            if (!whitelisted && typeof settings.throttle.bandwidth !== 'undefined') {
                if (typeof settings.throttle.bandwidth.user !== 'undefined')
                    pipe = pipe.pipe(user.throttleGroup.throttle(), pipesettings) // throttle for ip
                if (typeof settings.throttle.bandwidth.server !== 'undefined')
                    pipe = pipe.pipe(proxyServer.serverThrottleGroup.throttle(), pipesettings) // throttle for server
            }

            pipe.pipe(clientSocket, pipesettings); // pipe response

            pipe = clientSocket.pipe(proxyServer.fileSizeHandler.createRequestStream(cancelRequest), pipesettings); // maxBodySize checker

            if (!whitelisted && typeof settings.throttle.bandwidth !== 'undefined') {
                if (typeof settings.throttle.bandwidth.user !== 'undefined')
                    pipe = pipe.pipe(user.throttleGroup.throttle(), pipesettings) // throttle for ip
                if (typeof settings.throttle.bandwidth.server !== 'undefined')
                    pipe = pipe.pipe(proxyServer.serverThrottleGroup.throttle(), pipesettings); // throttle for server
            }

            pipe.pipe(destinationSocket, pipesettings); // pipe request

            destinationSocket.connect(parseInt(hostPort[1]), hostPort[0], () => {
                destinationSocket.write(bodyhead);
                clientSocket.write("HTTP/" + req.httpVersion + " 200 Connection established\r\n\r\n");
            });

            destinationSocket.on('end', () => {
                clientSocket.end();
            });

            destinationSocket.on('error', () => {
                clientSocket.write("HTTP/" + req.httpVersion + " 500 Connection error\r\n\r\n");
                clientSocket.end();
            });

            clientSocket.on('end', () => {
                destinationSocket.end();
            });

            clientSocket.on('error', () => {
                destinationSocket.end();
            });
        });
    });
}
module.exports = addHttpsProxy;
