const url = require('url');
const ip = require('ip');

const fileExtension = require('./util/FileExtension.js');
const getHostPortFromString = require('./util/HostPort.js');

/**
 * The access handler is used for checking whether requested resources are whitelisted
 * or blocked. All direct ip access is blocked unless whitelisted.
 * A whitelisted resource is immune from bandwith throttlig, request throttling,
 * and splash page redirection.
 * A blocked resource is prevented from being requested through the proxy under all
 * circumstances.
 * @param {Object} settings
 */
function AccessHandler(settings) {

    /**
     * Checks if the http request/response is an exception to throttling.
     * proxyRes has priority over req when selecting content-type to compare against
     * @param  {http.Request} req
     * @param  {http.Response} proxyRes
     * @return {Boolean}
     */
    this.isException = (req, proxyRes, hostPort) => {
        if (typeof settings.throttle.exception === 'undefined')
            return false;

        var contentType = (proxyRes || req).headers['content-type'];
        var path = url.parse(req.url).path;
        var fileType = fileExtension(path);

        // check contentTypes
        if (typeof contentType !== 'undefined')
            for (var str of settings.throttle.exception.contentTypes) {
                if (contentType.indexOf(str) !== -1)
                    return true;
            }

        // check fileTypes
        for (var str of settings.throttle.exception.fileTypes) {
            if (fileType === str)
                return true;
        }

        return false;
    };

    /**
     * Checks if the http request is whitelisted.
     * @param  {http.Request} req
     * @param  {Array} hostPort
     * @return {Boolean}
     */
    this.isWhitelisted = (req, hostPort) => {

        // check urls
        if (typeof req.url !== 'undefined')
            for (var reg of settings.whitelist.urls) {
                if ((new RegExp(reg[0], reg[1])).test(req.url))
                    return true;
            }

        // check hosts
        for (var reg of settings.whitelist.hosts) {
            if ((new RegExp(reg[0], reg[1])).test(hostPort[0]))
                return true;
        }

        return false;
    }

    /**
     * Checked to see if the http/https is blocked.
     * @param  {http.Request} req
     * @param  {Object} hostPort
     * @param  {String} serverIp
     * @param  {Boolean} whitelisted
     * @return {Boolean}
     */
    this.isBlocked = (req, hostPort, serverIp, whitelisted) => {
        // block all ip requests unless whitelisted
        if (hostPort[0] === 'localhost' || (!whitelisted && (ip.isV4Format(hostPort[0]) || ip.isV6Format(hostPort[0])))) {
            return true;
        }

        // check hosts
        for (var reg of settings.blocked.hosts) {
            if ((new RegExp(reg[0], reg[1])).test(hostPort[0]))
                return true;
        }

        // check urls
        for (var reg of settings.blocked.urls) {
            if ((new RegExp(reg[0], reg[1])).test(req.url))
                return true;
        }

        return false;
    }
}

module.exports = AccessHandler;
