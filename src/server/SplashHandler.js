/**
 * This is the function for handling the display of the splash page.
 * The splash page is shown inorder to inform the user of the proxy terms.
 * The user must be shown this page before they can access any page not on the
 * whitelist. The user is automatically redirected to the page specified by
 * settings.splash.url. Be sure to whitelist files on this page so that it loads
 * properly. The splash page redirection will be disabled when settings.splash.key
 * is requested and sent through the proxy. If this setting is undefined, redirection
 * will be disabled when settings.splash.url is sent through the proxy. Once disabled,
 * the splash will remain disabled until settings.splash.interval ms passes or the user
 * is removed from the user table.
 * @param {Object} settings
 */
function SplashHandler(settings) {

    /**
     * Determines if the user should be shown the splash screen. Updates the user's
     * lastSplash variable if the result is true.
     * @param  {User} user
     * @return {Boolean}
     */
    this.shouldSplash = (user) => {
        if (typeof settings.splash !== 'undefined') {
            const now = Date.now();
            if (user.lastSplashed + settings.splash.interval < now) {
                return true;
            }
        }
        return false;
    }

    /**
     * Called everytime a file is passed through the proxy to check if the splash key file
     * has been loaded and the user has agreed to the terms
     * @param  {String} url request url
     */
    this.onFileLoaded = (user, url) => {
        if (typeof settings.splash !== 'undefined') {
            if (typeof settings.splash.key !== 'undefined') {
                if (url === settings.splash.key) {
                    user.lashSplashed = Date.now();
                }
            } else {
                if (url === settings.splash.url) {
                    user.lashSplashed = Date.now();
                }
            }
        }
    }
}
module.exports = SplashHandler;
