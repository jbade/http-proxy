const stream = require('stream');
const url = require('url');

const fileExtension = require('./util/FileExtension.js');
const getHostPortFromString = require('./util/HostPort.js');

/**
 * The modifier handles the modification of data sent through the http proxy server.
 * It supports the modification of request/response headers and the request/response
 * body. Headers are modify before the request/response is relayed to its destination.
 * Bodies are modified as they are being relayed using streams.
 * Determining if data is to be modified is done by looking at content-type headers and
 * the file extension from the request url.
 */
function Modifier() {
    this._modifyResponseHeaders = [];
    this._modifyRequestHeaders = [];
    this._prependResponseBody = [];
    this._modifyResponseBody = [];
    this._prependRequestBody = [];
    this._modifyRequestBody = [];

    this.modify = (type, opt, func) => {
        var arr = null;
        switch (type) {
            case 'request-headers':
                arr = this._modifyRequestHeaders;
                break;
            case 'response-headers':
                arr = this._modifyResponseHeaders;
                break;
            case 'prepend-response-body':
                arr = this._prependResponseBody;
                break;
            case 'response-body':
                arr = this._modifyResponseBody;
                break;
            case 'prepend-request-body':
                arr = this._prependRequestBody;
                break;
            case 'request-body':
                arr = this._modifyRequestBody;
                break
            default:
                console.log('Incorrect varible type in modify()', type);
                process.exit(1);
                break;
        }

        arr.push({
            opt: opt,
            func: func
        });
    }

    this.shouldModify = (modifier, req, proxyRes) => {
        // must (match contentType or match fileTile) or (contentType and fileType are both null)
        /*
          Calls the function of the modifier if
            contentTypes are within the content-type headers
            fileTypes are equal to the fileExtension
              fileType is taken from the url
            contentTypes and fileTypes are null
         */
        var reqContentType = req.headers['content-type'];
        var resContentType = (typeof proxyRes === 'undefined') ? '' : proxyRes.headers['content-type']; // TODO: will this work
        var path = url.parse(req.url).path;
        var fileType = fileExtension(path);
        var cont = false;

        // modify if nothing specified
        if (!cont && (typeof modifier.opt.contentTypes === 'undefined' ||
                (typeof modifier.opt.contentTypes.request === 'undefined' &&
                    typeof modifier.opt.contentTypes.response === 'undefined')) &&
            typeof modifier.opt.fileTypes === 'undefined') {
            cont = true;

        }

        // check request contentTypes
        if (!cont && typeof reqContentType !== 'undefined' &&
            typeof modifier.opt.contentTypes !== 'undefined' &&
            typeof modifier.opt.contentTypes.request !== 'undefined') {
            modifier.opt.contentTypes.request.forEach((str) => {
                if (cont) return;
                if (reqContentType.indexOf(str) !== -1) {
                    cont = true;
                }
            });
        }

        // check response contentTypes
        if (!cont && typeof proxyRes !== 'undefined' && typeof resContentType !== 'undefined' &&
            typeof modifier.opt.contentTypes !== 'undefined' &&
            typeof modifier.opt.contentTypes.response !== 'undefined') {
            modifier.opt.contentTypes.response.forEach((str) => {
                if (cont) return;
                if (resContentType.indexOf(str) !== -1) {
                    cont = true;
                }
            });
        }

        // check fileType
        if (!cont && typeof fileType !== 'undefined' && typeof modifier.opt.fileTypes !== 'undefined') {
            modifier.opt.fileTypes.forEach((str) => {
                if (cont) return;
                if (fileType === str) {
                    cont = true;
                }
            });
        }

        return cont;
    }

    /**
     * Modifies the request headers
     * @param  {http.ClientRequest} req
     */
    this.modifyRequestHeaders = (req) => {
        this._modifyRequestHeaders.forEach((modifier) => {
            if (this.shouldModify(modifier, req)) {
                modifier.func(req);
            }
        });
    }

    /**
     *  Modifies the response headers
     * @param  {http.ClientRequest} req
     * @param  {http.ServerResponse} proxyRes
     */
    this.modifyResponseHeaders = (req, proxyRes) => {
        this._modifyResponseHeaders.forEach((modifier) => {
            if (this.shouldModify(modifier, req, proxyRes)) {
                modifier.func(req, proxyRes);
            }
        });
    }

    this.prependResponseBody = (req, proxyRes) => {
        var data = '';

        this._prependResponseBody.forEach((modifier) => {
            if (this.shouldModify(modifier, req, proxyRes)) {
                data += modifier.func(req, proxyRes);
            }
        });
        return data;
    }

    this.prependRequestBody = (req) => {
        var data = '';

        this._prependRequestBody.forEach((modifier) => {
            if (this.shouldModify(modifier, req)) {
                data += modifier.func(req);
            }
        });
        return data;
    }

    this.createModifyResponseBodyStream = (req, proxyRes) => {
        var pipe = new stream.Transform({
            objectMode: true
        });

        pipe._transform = (chunk, encoding, done) => {
            this._modifyResponseBody.forEach((modifier) => {
                if (this.shouldModify(modifier, req, proxyRes)) {
                    chunk = modifier.func(req, proxyRes, chunk, encoding);
                }
            });
            pipe.push(chunk);
            done();
        };

        return pipe;
    }

    this.createModifyRequestBodyStream = (req) => {
        var pipe = new stream.Transform({
            objectMode: true
        });

        pipe._transform = (chunk, encoding, done) => {
            this._modifyRequestBody.forEach((modifier) => {
                if (this.shouldModify(modifier, req)) {
                    chunk = modifier.func(req, chunk, encoding);
                }
            });
            pipe.push(chunk);
            done();
        };

        return pipe;
    }
}


module.exports = Modifier;
