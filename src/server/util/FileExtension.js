var regExp_fileExtension = /^.+\.([^.]+)$/;

/**
 * Determines the file extension from a url path. Returns '' if unable to determine
 * the file extension.
 * @param  {String} path
 * @return {String}
 */
function fileExtension(path) {
    var ext = regExp_fileExtension.exec(path);
    if (ext === null) return '';
    var questionIndex = ext[1].indexOf('?');
    return questionIndex === -1 ? ext[1] : ext[1].substring(0, questionIndex);
}

module.exports = fileExtension;
