var regex_hostport = /^([^:]+)(:([0-9]+))?$/;

/**
 * Gets the host and port from a string and returns an array [host, port]
 * If no port is present in the hostString, the deafaultPort will be placed in the
 * array.
 * @param  {String} hostString
 * @param  {Number} defaultPort
 * @return {Array}
 */
function getHostPortFromString(hostString, defaultPort) {
    var host = hostString;
    var port = defaultPort;

    var result = regex_hostport.exec(hostString);
    if (result != null) {
        host = result[1];
        if (result[2] != null) {
            port = result[3];
        }
    }

    return ([host, port]);
}

module.exports = getHostPortFromString;
