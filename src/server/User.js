const ThrottleGroup = require('./util/throttle.js').ThrottleGroup;
const RateLimiter = require('./util/RateLimiter.js');
const TransferTracker = require('./TransferTracker.js');

/**
 * User is used to store data that is unique for each proxy user.
 * @param {Object} settings
 * @param {String} key      key in UserTable
 */
function User(settings, key) {
    this.key = key; // key in UserTable
    this.lastAccessed = Date.now();
    this.lastSplashed = 0; // last time the user has been redirected to the splash page
    if (typeof settings.throttle.bandwidth !== 'undefined' && typeof settings.throttle.bandwidth.user !== 'undefined')
        this.throttleGroup = new ThrottleGroup({
            burstRate: settings.throttle.bandwidth.user.burstRate,
            fillRate: settings.throttle.bandwidth.user.fillRate,
            chunksize: settings.throttle.bandwidth.user.chunksize
        });
    this.requestLimiter = new RateLimiter(settings.throttle.requests.user.burstRate, settings.throttle.requests.user.fillRate, settings.throttle.requests.user.interval, true);
    this.transferTracker = new TransferTracker(settings);
}

module.exports = User;
