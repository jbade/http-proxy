const stream = require('stream');

/**
 * The file size handler is used for checking/monitoring the size of the data
 * transfered through the proxy. This is done by checking the headers on requests
 * and responses and counting the bytes that pass through a pipe.
 * In the event that the size of the data exceeds the specified threshold, the
 * request should be closed.
 * @param {Object} settings
 */
function FileSizeHandler(settings) {

    /**
     * Creates a stream that will pipe contents until a maximum number of bytes of data are sent.
     * @param  {Number} maxSize Max number of bytes the pipe should send
     * @param  {Function} cancel  Function to call if the max has been hit
     * @return {stream.Transform}
     */
    this.createFileSizeLimitingStream = (maxSize, cancel) => {
        var currentSize = 0;
        var pipe = new stream.Transform({
            objectMode: true
        });

        pipe._transform = (chunk, encoding, done) => {
            currentSize += chunk.length;
            if (currentSize > maxSize) {
                console.log('FileSizeLimitingStream canceling ', currentSize);
                cancel();
                return;
            }

            pipe.push(chunk);
            done();
        };

        return pipe;
    }

    /**
     * Creates a stream that the request should be piped through.
     * @param  {Function} cancel Function to call to cancel the request
     * @return {stream}
     */
    this.createRequestStream = (cancel) => {
        if (typeof settings.throttle.filesize !== 'undefined' && typeof settings.throttle.filesize.request !== 'undefined') {
            return this.createFileSizeLimitingStream(settings.throttle.filesize.request, cancel);
        } else {
            return new stream.Transform({
                objectMode: true
            });
        }
    }

    /**
     * Creates a stream that the response should be piped through.
     * @param  {Function} cancel Function to call to cancel the request
     * @return {stream}
     */
    this.createResponseStream = (cancel) => {
        if (typeof settings.throttle.filesize !== 'undefined' && typeof settings.throttle.filesize.response !== 'undefined') {
            return this.createFileSizeLimitingStream(settings.throttle.filesize.response, cancel);
        } else {
            return new stream.Transform({
                objectMode: true
            });
        }
    }


    /**
     * Checks to make sure the request header content-length does not exceed the option maxTransfer
     * @param  {http.ClientRequest} req
     * @return {Boolean}     if the request should be denied
     */
    this.checkRequestHeaders = (req) => {
        if (typeof settings.throttle.filesize !== 'undefined' && typeof settings.throttle.filesize.request !== 'undefined' && typeof req.headers['content-length'] !== 'undefined') {
            //console.log('request', req.headers['content-length'], settings.throttle.body.request);
            if (parseInt(req.headers['content-length']) > settings.throttle.filesize.request) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks to make sure the response header content-length does not exceed the option maxTransfer
     * @param  {http.ServerResponse} res
     * @return {Boolean}     if the request/response should be denied
     */
    this.checkResponseHeaders = (res) => {
        if (typeof settings.throttle.filesize !== 'undefined' && typeof settings.throttle.filesize.response !== 'undefined' && typeof res.headers['content-length'] !== 'undefined') {
            //console.log('response', res.headers['content-length'], settings.throttle.filesize.response);
            if (parseInt(res.headers['content-length']) > settings.throttle.filesize.response) {
                return true;
            }
        }
        return false;
    }

}

module.exports = FileSizeHandler;
