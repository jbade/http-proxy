const User = require('./User.js');

/**
 * The UserTable holds tracking objects for every user. The table is automatically
 * cleaned every 10 minutes by removing users that have been inactive for 24 hours.
 * After a user has been removed from the table, if they use the proxy again, they
 * will be seen as a new user.
 * @param {Object} options ProxySever options
 */
function UserTable(settings) {
    this.table = {};

    /**
     * Add the specified user to the table if not already present
     * @param  {String} user A unique identified for the user. (ip address)
     * @return {Boolean}      If the table was modified.
     */
    this.add = (user) => {
        if (typeof this.table[user] !== 'undefined')
            return false;

        this.table[user] = new User(settings, user);

        return true;
    }

    /**
     * Gets the specified user to the table. Creates a new User if not present.
     * @param  {String} user A unique identified for the user. (ip address)
     * @return {User}
     */
    this.get = (user) => {
        if (typeof this.table[user] === 'undefined') {
            this.add(user);
        }
        return this.table[user];
    }

    /**
     * Deletes users that have been inactive for more than one day from the user table
     */
    this.clean = () => {
        const now = Date.now();
        for (var id in this.table) {
            if (this.table[id].lastAccessed + 24 * 60 * 60 * 1000 < now) {
                delete this.table[id];
            }
        }
    }

    // clean every 10 minutes
    setInterval(this.clean, 10 * 60 * 1000);
}

module.exports = UserTable;
