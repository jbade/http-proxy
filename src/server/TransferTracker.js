/**
 * The transfer tracker is used to track how much data has passed through through
 * the proxy for a specific user. This metric is used to block users who exceed
 * a data transfer threshold. The user is permited a specific amount of data in
 * specific intervals. If they use all of their alloted data during an interval,
 * they will be unable to use any more data until the next interval.
 * @param {Object} settings
 */
function TransferTracker(settings) {
    this.stage = 0; // current chunk in time that the request totals are representing
    // stage = time in ms / interval
    this.requestTotal = 0;
    this.responseTotal = 0;

    this.add = (requestTransfer, responseTransfer) => {
        this.requestTotal += requestTransfer;
        this.responseTotal += responseTransfer;
        this.total = this.requestTotal + this.responseTotal;
    }

    this.reset = () => {
        this.requestTotal = 0;
        this.responseTotal = 0;
        this.total = 0;
        this.stage = Math.floor(Date.now() / settings.throttle.transfer.user.interval);
    }

    this.resetIfNecessary = () => {
        if (this.stage !== Math.floor(Date.now() / settings.throttle.transfer.user.interval)) {
            this.reset();
        }
    }

    this.isLimited = () => {
        this.resetIfNecessary();
        return this.total >= settings.throttle.transfer.user.size;
    }

}

module.exports = TransferTracker;
