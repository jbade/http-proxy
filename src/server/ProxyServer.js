const http = require('http');
const url = require('url');
const ip = require('ip');
const zlib = require('zlib');
const fs = require('fs');
const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const path = require('path');

const ThrottleGroup = require('./util/throttle.js').ThrottleGroup;
const addHttpsProxy = require('./HttpsAddon.js');
const getHostPortFromString = require('./util/HostPort.js');
const Modifier = require('./Modifier.js');
const FileSizeHandler = require('./FileSizeHandler.js');
const UserTable = require('./UserTable.js');
const AccessHandler = require('./AccessHandler.js');
const RedirectHandler = require('./RedirectHandler.js');
const SplashHandler = require('./SplashHandler.js');

/**
 * HTTP/HTTPS proxy server
 *
 * @param {Object} settings
     servers: [{  // Array of the servers the proxy server should open
         port: Number,      // port to listen on (required)
         https: Boolean     // if https requests should be allowed (default = true)
     }],
     throttle: {  // Throttle settings (all required)
         bandwidth: { // Throttle the speed at which data is passed through the server
             server: {  // bucket for the whole server
                 burstRate: Number, // The maximum drain rate in bytes/s
                 fillRate: Number, // The fill rate in bytes/s
                 chunksize: Number // The maximum chunk size into which larger writes are decomposed
             },
             user: {  // bucket for individual users
                 burstRate: Number, // The maximum drain rate in bytes/s
                 fillRate: Number, // The fill rate in bytes/s
                 chunksize: Number // The maximum chunk size into which larger writes are decomposed
             }
         },
         requests: { // Throttle the speed at which requests can be made
             user: { // bucket for individual users
                 burstRate: Number, // The maximum drain rate in requests/interval
                 fillRate: Number, // The fill rate in requests/interval
                 interval: Number // The time in which the rates are based in ms
             }
         },
         filesize: { // Maximum body lengths (The maximum file size a user can upload/download)
             request: Number, // Maximum request body length in bytes
             response: Number // Maximum response body length in bytes
         },
         transfer: { // The amount of data permited through the proxy server
             user: { // Amount of data permited per user
                 size: Number, // Amount of data allowed in bytes
                 interval: Number // The amount of time alloted in ms for the specified amount of data
             }
         },
         // applies to bandwidth, requests. does not apply to filesize, transfer
         whitelist: { // Exceptions to bandwidth and request throttling. Only one specifier must match for
                      // a request to be whitelisted
             http: { // Applicable to http requests
                 contentTypes: [String], // Part of the content-type header
                 fileTypes: [String], // The file extension taken from the url
                 urls: [[String, String]] // Regex to test on urls /[0]/[1]
                 hosts: [[String, String]], // Regex to test on hosts /[0]/[1]
             },
             https: { // Applicable to https requests
                 hosts: [[String, String]], // Regex to test on hosts /[0]/[1]
             }
         }
     },
     splash: { // Users are shown a splash screen before they are able to access nonwhitelisted sites (optional)
         // be sure to whitelist all files on splashpage
         url: String, // The url of the page the user will be redirected to
         key: '', // The url of the key file that must be loaded to signify the user has seen the splash page
         interval: 10 * 60 * 1000 // The interval that the splash page will reappear to users in ms
     },
     redirects: { // Urls to redirect the user to when they encounter the specified state (optional)
         blocked: String, // When the request is blocked
         transferlimit: String, // When the user exceeds the transfer throttle
         requestlimit: String, // When the user exceeds the request throttle
         filesizelimit: String // When the user exceeds the file size throttle
     },
     blocked: { // Blocked resources that will not be permitted through the proxy server
         hosts: [[String, String]], // Regex to test on hosts /[0]/[1]
         urls: [[String, String]] // Regex to test on urls /[0]/[1]
     }
 */
function ProxyServer(settings) {

    this.servers = [];

    this.accessHandler = new AccessHandler(settings);
    this.userTable = new UserTable(settings);
    this.modifier = new Modifier();
    if (typeof settings.throttle.bandwidth !== 'undefined' && typeof settings.throttle.bandwidth.server !== 'undefined')
        this.serverThrottleGroup = new ThrottleGroup({
            burstRate: settings.throttle.bandwidth.server.burstRate,
            fillRate: settings.throttle.bandwidth.server.fillRate,
            chunksize: settings.throttle.bandwidth.server.chunksize
        });
    this.fileSizeHandler = new FileSizeHandler(settings);
    this.redirectHandler = new RedirectHandler(settings);
    this.splashHandler = new SplashHandler(settings);

    this.modify = (type, opt, func) => {
        this.modifier.modify(type, opt, func);
    }

    /**
     * Creates a proxy server from the provided options
     * @param  {Object} options {port: Number, https: boolean}
     */
    this.createServer = (options) => {
        var server = http.createServer((req, res) => {

            if (typeof req.headers['host'] === 'undefined' && typeof req.url === 'undefined') {
                res.end();
                return;
            }

            // attempt to standardize request formating
            if (typeof req.headers['host'] === 'undefined') {
                var parsed = url.parse(req.url);
                req.headers['host'] = parsed.host;
            }
            if (typeof req.url === 'undefined') {
                req.url = '/';
            }
            if (req.url.indexOf('http://') !== 0) {
                req.url = url.resolve('http://' + req.headers['host'], req.url);
            }

            var requestIp = req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;

            if (typeof settings.logger !== 'undefined')
                settings.logger.log('info', {
                    type: 'request',
                    ip: requestIp,
                    host: req.headers['host'],
                    url: req.url
                });


            var proxyRes = null;
            var proxyReq = null;
            var user = this.userTable.get(requestIp);
            var hostPort = getHostPortFromString(req.headers['host'], 80);
            var whitelisted = this.accessHandler.isWhitelisted(req, hostPort);
            var exception = this.accessHandler.isException(req, proxyRes, hostPort);

            // handle blocking
            if (this.accessHandler.isBlocked(req, hostPort, server.address().address, whitelisted)) {
                this.redirectHandler.blocked(res);
                return;
            }


            // console.log(whitelisted);
            // if (!whitelisted) {
            //     res.end();
            //     return;
            // }




            // handle splash
            if (!whitelisted && this.splashHandler.shouldSplash(user)) {
                this.redirectHandler.splash(res);
                return;
            }

            // handle transfer limits
            if (user.transferTracker.isLimited()) {
                this.redirectHandler.transferLimit(res);
                return;
            }

            var cancelRequest = () => {
                if (proxyReq !== null)
                    proxyReq.abort();
                res.end();
            };

            user.requestLimiter.removeTokens(1, (err, remainingRequests) => {
                if (!whitelisted && !exception && (err || remainingRequests < 0)) { // cancel request if hit rate limit
                    this.redirectHandler.requestLimit(res);
                    return;
                }

                if (!whitelisted && this.fileSizeHandler.checkRequestHeaders(req)) { // check header for maxBodySize
                    this.redirectHandler.fileSizeLimit(res);
                    return;
                }

                this.modifier.modifyRequestHeaders(req);

                // update accept-encoding to what we support
                if (typeof req.headers['accept-encoding'] === 'string')
                    req.headers['accept-encoding'] = req.headers['accept-encoding'].split(',').map((encoding) => {
                        return encoding.trim();
                    }).filter((encoding) => {
                        return encoding === 'gzip' || encoding === 'deflate';
                    }).join(', ');

                // TODO: wtf. does body have an encoding. gzip/deflate???

                delete req.headers['content-size'];

                var pipesettings = {
                    end: true
                };

                proxyReq = http.request({
                    host: hostPort[0],
                    port: hostPort[1],
                    path: req.url.substring(7 + req.headers['host'].length), // 7 = 'http://'.length
                    method: req.method,
                    headers: req.headers
                }, (proxyRes2) => {
                    proxyRes = proxyRes2;
                    whitelisted = this.accessHandler.isWhitelisted(req, hostPort);

                    if (!whitelisted && this.fileSizeHandler.checkResponseHeaders(proxyRes)) { // check header for maxBodySize
                        proxyReq.abort();
                        this.redirectHandler.fileSizeLimit(res);
                        return;
                    }

                    // TODO: should this listener be on res instead of proxyRes???
                    // add listener for tracking total data transfered
                    proxyRes.on('end', () => {
                        var requestTransfer = proxyRes.socket._bytesDispatched;
                        var responseTransfer = proxyRes.socket.bytesRead;
                        user.transferTracker.add(requestTransfer, responseTransfer);
                        this.splashHandler.onFileLoaded(user, req.url);
                    });

                    // handle zipped responses
                    var responseEncoding = proxyRes.headers['content-encoding']; // grab encoding before it can be modified
                    var shouldZip = typeof responseEncoding === 'string';

                    this.modifier.modifyResponseHeaders(req, proxyRes);

                    delete proxyRes.headers['content-size'];
                    delete proxyRes.headers['content-length'];

                    // TODO: should we allow response encoding to be modified?
                    if (typeof responseEncoding !== 'undefined') {
                        proxyRes.headers['content-encoding'] = responseEncoding;
                    } else {
                        delete proxyRes.headers['content-encoding'];
                    }


                    //console.log(proxyRes.headers);
                    res.writeHead(proxyRes.statusCode, proxyRes.statusMessage, proxyRes.headers);

                    var start = this.fileSizeHandler.createResponseStream(cancelRequest); // maxBodySize checker
                    var pipe = start;

                    // throttling
                    if (!whitelisted && !exception && typeof settings.throttle.bandwidth !== 'undefined') {
                        if (typeof settings.throttle.bandwidth.user !== 'undefined')
                            pipe = pipe.pipe(user.throttleGroup.throttle(), pipesettings); // throttle for ip
                        if (typeof settings.throttle.bandwidth.server !== 'undefined')
                            pipe = pipe.pipe(this.serverThrottleGroup.throttle(), pipesettings) // throttle for server
                    }

                    // unzipping
                    if (shouldZip) {
                        pipe = pipe.pipe(responseEncoding === 'gzip' ? zlib.createGunzip() : zlib.createInflate());
                    }

                    // modifying
                    pipe = pipe.pipe(this.modifier.createModifyResponseBodyStream(req, proxyRes), pipesettings); // modify body

                    // rezipping
                    if (shouldZip) {
                        pipe = pipe.pipe(responseEncoding === 'gzip' ? zlib.createGzip() : zlib.createDeflate());
                    }

                    // response
                    pipe.pipe(res, pipesettings);

                    if (shouldZip) {
                        pipe.write(this.modifier.prependResponseBody(req, proxyRes));
                    } else {
                        res.write(this.modifier.prependResponseBody(req, proxyRes));
                    }

                    proxyRes.pipe(start, pipesettings);
                });

                proxyReq.on('error', function(e) {
                    cancelRequest();
                });

                var start = this.fileSizeHandler.createRequestStream(cancelRequest); // maxBodySize checker
                var pipe = start;

                // throttling
                if (!whitelisted && !exception && typeof settings.throttle.bandwidth !== 'undefined') {
                    if (typeof settings.throttle.bandwidth.user !== 'undefined')
                        pipe = pipe.pipe(user.throttleGroup.throttle(), pipesettings) // throttle for ip
                    if (typeof settings.throttle.bandwidth.server !== 'undefined')
                        pipe = pipe.pipe(this.serverThrottleGroup.throttle(), pipesettings); // throttle for server
                }

                pipe = pipe.pipe(this.modifier.createModifyRequestBodyStream(req), pipesettings) // modify body
                    .pipe(proxyReq, pipesettings); // send request

                start.write(this.modifier.prependRequestBody(req));

                req.pipe(start, pipesettings);
            });
        }).listen(options.port);

        //https proxy server
        if (typeof options.https === 'undefined' || options.https === true) {
            addHttpsProxy(settings, server, this);
        }

        this.servers.push(server);

        if (typeof settings.logger !== 'undefined')
            settings.logger.log('info', {
                type: 'proxy',
                port: options.port,
                https: (typeof options.https === 'undefined' || options.https)
            });
    }

    // start the specified servers
    settings.servers.forEach((server) => {
        this.createServer(server);
    })
}

module.exports = ProxyServer;
