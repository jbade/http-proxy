const request = require('request');
const ip = require('ip');

/**
 * Submits the running proxy servers to various free proxy lists by running them
 * through public proxy checkers.
 * @param {Number} interval   ms
 * @param {ProxyServer} proxyServer
 * @param {?} logger
 */
function Submitter(interval, proxyServer, logger) {

    /**
     * Submits proxy to various proxy checking websites in hope of being added to proxy lists
     */
    this.submit = () => {
        var proxies = proxyServer.servers.map((server) => {
            const addr = server.address();
            return {
                ip: ip.address(),
                port: addr.port
            }; // TODO: figure out how to get all external ips
        });

        var linelist = proxies.map((proxy) => {
            return proxy.ip + ':' + proxy.port;
        }).join('\r\n');


        request.post({
            url: 'http://urlchecker.org/ajax/proxy_parser.php',
            form: {
                'data': linelist,
                'code': ''
            },
            headers: {
                'Host': 'urlchecker.org',
                'Accept': 'application/json, text/javascript, *',
                'Origin': 'http://urlchecker.org',
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Referer': 'http://urlchecker.org/proxy/?proxy'
            }
        }, (err) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'urlchecker.org',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://www.proxy-checker.org/result.php',
            form: {
                'list': linelist
            },
            headers: {
                'Cache-Control': 'max-age=0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Host': 'www.proxy-checker.org',
                'Origin': 'http://www.proxy-checker.org',
                'Upgrade-Insecure-Requests': '1',
                'Referer': 'http://www.proxy-checker.org/'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'www.proxy-checker.org',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'https://xnode.org/page/Proxy_Checker',
            form: {
                'entry': linelist
            },
            headers: {
                'Cache-Control': 'max-age=0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Host': 'xnode.org',
                'Origin': 'https://xnode.org',
                'Upgrade-Insecure-Requests': '1',
                'Referer': 'https://xnode.org/page/Proxy_Checker'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'xnode.org',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://www.proxyfarm.com/index.php',
            form: {
                'proxies': linelist,
                'action': 'fred'
            },
            headers: {
                'Cache-Control': 'max-age=0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Host': 'www.proxyfarm.com',
                'Origin': 'http://www.proxyfarm.com',
                'Upgrade-Insecure-Requests': '1',
                'Referer': 'http://www.proxyfarm.com/'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'www.proxyfarm.com',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://proxyorca.com/index.php?act=proxy-checker',
            form: {
                'sendaccounts': linelist,
                'timeout': 7
            },
            headers: {
                'Cache-Control': 'max-age=0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Host': 'proxyorca.com',
                'Origin': 'http://proxyorca.com',
                'Upgrade-Insecure-Requests': '1',
                'Referer': 'http://proxyorca.com/index.php?act=proxy-checker'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'proxyorca.com',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://proxyipchecker.com/ws.php',
            form: {
                'proxylist': linelist
            },
            headers: {
                'Cache-Control': 'max-age=0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Host': 'proxyipchecker.com',
                'Origin': 'http://proxyipchecker.com',
                'Upgrade-Insecure-Requests': '1',
                'Referer': 'http://proxyipchecker.com/'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'proxyipchecker.com',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://www.proxy-checker.org/result.php',
            form: {
                'list': linelist
            },
            headers: {
                'Cache-Control': 'max-age=0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Host': 'www.proxy-checker.org',
                'Origin': 'http://www.proxy-checker.org',
                'Upgrade-Insecure-Requests': '1',
                'Referer': 'http://www.proxy-checker.org/'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'www.proxy-checker.org',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://proxyhttp.net/check',
            form: {
                'i': linelist,
                'info': 'full'
            },
            headers: {
                'Host': 'proxyhttp.net',
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Origin': 'http://proxyhttp.net',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Referer': 'http://proxyhttp.net/check',
                'Accept-Language': 'en-US,en;q=0.8',
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'proxyhttp.net',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://checkerproxy.net/step1',
            form: {
                'data': linelist,
                'type': '0',
                'timeout': '20',
                'notSave': '0'
            },
            headers: {
                'Host': 'checkerproxy.net',
                'Accept': '*/*',
                'Accept-Language': 'en-US,en;q=0.8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Origin': 'http://checkerproxy.net',
                'Referer': 'http://checkerproxy.net/'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'checkerproxy.net',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'https://incloak.com/api/checker.php?out=js&action=list_new&tasks=http,ssl,socks4,socks5&parser=lines',
            form: {
                'data': linelist
            },
            headers: {
                'Host': 'incloak.com',
                'Accept': '*/*',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Referer': 'https://incloak.com/proxy-checker/',
                'Orgin': 'https://incloak.com',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept-Language': 'en-US,en;q=0.8'
            }
        }, (err, res, body) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'incloak.com',
                    proxies: proxies,
                    error: err
                });
        });

        request.post({
            url: 'http://www.checker.freeproxy.ru/engine/parser.php',
            form: {
                'data': linelist
            },
            headers: {
                'Host': 'www.checker.freeproxy.ru',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Origin': 'http://www.checker.freeproxy.ru',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                'Referer': 'http://www.checker.freeproxy.ru/checker/',
                'Orgin': 'http://www.checker.freeproxy.ru',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Accept-Language': 'en-US,en;q=0.8',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }, (err) => {
            if (typeof logger !== 'undefined')
                logger.log('info', {
                    type: 'submit',
                    host: 'www.checker.freeproxy.ru',
                    proxies: proxies,
                    error: err
                });
        });

        proxies.forEach((proxy) => {
            request.post({
                url: 'https://www.proxicity.io/services/check-proxy',
                form: {
                    'ip': proxy.ip,
                    'port': proxy.port
                },
                headers: {
                    'Host': 'www.proxicity.io',
                    'Accept': '*/*',
                    'Origin': 'https://www.proxicity.io',
                    'X-Requested-With': 'XMLHttpRequest',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Referer': 'https://www.proxicity.io/proxy-checker'
                }
            }, (err) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'www.proxicity.io',
                        proxy: proxy,
                        error: err
                    });
            });

            request.post({
                url: 'http://ipaddress.com/proxy-checker/',
                form: {
                    'proxy': proxy.ip + ':' + proxy.port
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'ipaddress.com',
                    'Origin': 'http://ipaddress.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://ipaddress.com/proxy-checker/'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'ipaddress.com',
                        proxy: proxy,
                        error: err
                    });
            });

            request.post({
                url: 'http://www.pdproxy.com/proxy-checker.htm',
                form: {
                    'ipport': proxy.ip + ':' + proxy.port
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'www.pdproxy.com',
                    'Origin': 'http://www.pdproxy.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://www.pdproxy.com/proxy-checker.htm'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'www.pdproxy.com',
                        proxy: proxy,
                        error: err
                    });
            });


            request.post({
                url: 'http://www.aliveproxy.com/proxy-checker/default.aspx',
                form: {
                    'I': proxy.ip,
                    'P': proxy.port,
                    'submit1': 'Checking proxy'
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'www.aliveproxy.com',
                    'Origin': 'http://www.aliveproxy.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://www.aliveproxy.com/proxy-checker/'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'www.aliveproxy.com',
                        proxy: proxy,
                        error: err
                    });
            });

            request.post({
                url: 'http://samair.ru/s-proxychecker/index.php',
                form: {
                    'proxy': proxy.ip + ':' + proxy.port,
                    'time': '10',
                    'go': 'check'
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'samair.ru',
                    'Origin': 'http://samair.ru',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://samair.ru/s-proxychecker/index.php'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'samair.ru',
                        proxy: proxy,
                        error: err
                    });
            });

            request.post({
                url: 'http://www.test-net.org/services/proxy-checker/?host=1.1.1.1&port=8008&submit=Check+proxy',
                form: {
                    'host': proxy.ip,
                    'port': proxy.port,
                    'submit': 'Check proxy'
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'www.test-net.org',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://www.test-net.org/services/proxy-checker/'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'www.test-net.org',
                        proxy: proxy,
                        error: err
                    });
            });

            request.post({
                url: 'http://www.infobyip.com/proxychecker.php',
                form: {
                    'url': proxy.ip + ':' + proxy.port,
                    'type': 'HTTP'
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'www.infobyip.com',
                    'Origin': 'http://www.infobyip.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://www.infobyip.com/proxychecker.php'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'www.infobyip.com',
                        proxy: proxy,
                        error: err
                    });
            });

            request.post({
                url: 'http://ping.eu/action.php?atype=7',
                form: {
                    'host': proxy.ip,
                    'port': proxy.port,
                    'go': 'Go'
                },
                headers: {
                    'Cache-Control': 'max-age=0',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
                    'Host': 'ping.eu',
                    'Origin': 'http://ping.eu',
                    'Upgrade-Insecure-Requests': '1',
                    'Referer': 'http://ping.eu/proxy/'
                }
            }, (err, res, body) => {
                if (typeof logger !== 'undefined')
                    logger.log('info', {
                        type: 'submit',
                        host: 'ping.eu',
                        proxy: proxy,
                        error: err
                    });
            });
        });
    }

    // start submit process
    setInterval(this.submit, interval);
    this.submit();

}

module.exports = Submitter;
